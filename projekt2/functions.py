import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sst
import scipy.signal as sig


from IPython.display import set_matplotlib_formats
set_matplotlib_formats( 'svg')

def one_slope(input_file, d_start, d_end):
    # Read raw measurement data 
    with open(input_file) as ff:
        spectrum = ff.read().split()
    if d_start > d_end: # reverse the data so the distance would be increasing
        spectrum = [x for x in reversed(spectrum)]
        d_start, d_end = d_end, d_start
    screens_dBm = []
    spectrum = spectrum[20:]
    for line in spectrum:
        screens_dBm.append( [float(x) for x in line.split(';')]   )
    d_axis = np.linspace(d_start, d_end, len(screens_dBm))
    # Extract information from dataset
    peaks_dBm = np.amax(np.array(screens_dBm), 1) # maximum received intensity from all   frequencies
    Losses_dBm = peaks_dBm[0] - peaks_dBm # normalize to max intensity
    b,a = sig.bessel(2, 1/15) # Bessel filter for removing fast fading - unlike EMA, doesn't lag behind data
    Losses_filtered_dBm = sig.lfilter(b, a, Losses_dBm)
    plt.figure()
    plt.title("Losses without fast fading")
    plt.plot(d_axis, Losses_dBm, label=f'Losses [dBm]')
    plt.plot(d_axis, Losses_filtered_dBm, label=f'Filtered losses [dBm]')
    plt.xlabel('distance [m]')
    plt.ylabel('path loss [dB]')
    plt.legend()
    plt.show()
    # Apply one-slope model
    logd = np.log10(d_axis)
    n, L1, rv, pv, stderr = sst.linregress(logd, Losses_dBm) # fit the one-slope model
    Lp = L1 + n * logd # prediction by the derived model
    err = Lp-Losses_dBm
    stdev = np.std(err)
    # Plot data
    fig, ax = plt.subplots(1,1,figsize=(9, 5))
    plt.semilogx(d_axis, Losses_dBm, '+', lw=0.1, label='measurement')
    ax.fill_between(d_axis, (Lp-stdev), (Lp+stdev), color='b', alpha=.1)
    plt.semilogx(d_axis, Lp, lw=3, label=f'1SM: L1 = {L1:.1f} dB, n = {0.1*n:.1f} dB')
    plt.semilogx(d_axis, Lp, lw=3, color='b', alpha=.1, label=f'1$\,\sigma$ confidence interval')
    plt.xlabel('distance [m]')
    plt.ylabel('path loss [dB]')
    plt.title('Path loss prediction vs. measurement')
    plt.legend()
    plt.grid()
    plt.show()
    # Print quantities
    print('Empirical One Slope Model parameters:')
    print('L1 =', L1.round(1), 'dB')
    print('n =', (0.1*n).round(1))
    print('standard deviation =', stdev.round(1), 'dB')
    print('mean error =', np.mean(np.abs(err)).round(1), 'dB')
    # Print histogram of error
    plt.figure()
    plt.hist(err, bins=70, density=True, label='Empirical PDF')
    # Compare with log-normal distribution
    pdfx = np.linspace(-30,30)
    ppdf = sst.norm.pdf(pdfx, scale=stdev)
    plt.plot(pdfx, ppdf, label='Normal distribution fit')
    plt.xlabel('loss error [dB]')
    plt.ylabel('count [-]')
    plt.legend()
    plt.grid()
    plt.savefig("asdferr.svg")
    plt.show()
    # Perform tests of normality
    shapiro_test = sst.shapiro(err)
    print("Shapiro-Wilk test p-value: {}".format(1-shapiro_test.pvalue))
    return Losses_dBm, err, stdev, d_axis

def ecdf(data, normalize=-999.0):
    '''
    generates CDF from empirical data 
    data - empirical data as a numpy vector
    normalize - percentil (%) to set zero (no normalization if outside <0,100>)
    returns - (x, y) vectors in tuple    
    '''
    x = np.sort(data)
    if 0 <= normalize <= 100.0: 
        x -= np.percentile(data, normalize)
    y = np.arange(1, len(x)+1)/float(len(x))
    return (x, y)
def plot_ecdf(err, stdev):
    plt.figure(figsize=(9, 7))
    xcdf, ycdf = ecdf(err, 50)
    plt.semilogy(xcdf, ycdf, label='Empirical CDF')
    plt.semilogy(xcdf, sst.norm.cdf(xcdf,scale=stdev), label='Normal CDF fit')
    plt.ylim(0.001,1)
    plt.xlabel('Normalized predicted RSS [dB]')
    plt.ylabel('Share of locations RSS < predicted RSS [-]')
    plt.legend()
    plt.grid()
    plt.show()

def plot_deciles(L, stdev, err, d):
    logd = np.log10(d)
    Lp = L + err # prediction by the derived model
    Pn = 20
    plt.figure(figsize=(9, 9))
    plt.subplot(211)
    plt.semilogx(d, Pn-L, '+')
    plt.plot(d, Pn-Lp, lw =4, label='50 % location exceeding this' )
    plt.plot(d, Pn-Lp-9.5, lw =4, label='90 % location exceeding this' )
    plt.plot(d, Pn-Lp+9.5, lw =4, label='10 % location exceeding this' )
    plt.xlabel('Distance [m]')
    plt.ylabel('RSS [dBm]')
    plt.title('Predicted RSS')
    plt.legend()
    plt.grid()
    plt.subplot(212)
    plt.plot(d, Pn-L, '+')
    plt.plot(d, Pn-Lp, lw =4, label='50 % location exceeding this' )
    plt.plot(d, Pn-Lp-9.5, lw =4, label='90 % location exceeding this' )
    plt.plot(d, Pn-Lp+9.5, lw =4, label='10 % location exceeding this' )
    plt.xlabel('Distance [m]')
    plt.ylabel('RSS [dBm]')
    plt.legend()
    plt.grid()
    plt.show()

# Reading raw measurement data 
def stationary_data(datasets, time, ignore_samples, labels):
    fig, axs = plt.subplots(len(datasets), 1, figsize=(9, len(datasets)*5))
    Rx = []
    t = []
    for i, f in enumerate(datasets):
        Rx.append([])
        with open(f) as ff:
            for line in ff.read().split():                 
                Rx[i].append(max([float(x) for x in line.split(';')])) # extracting RX value from raw data as a maximum value of each sweep screen     
        Rx[i] = Rx[i] - np.median(Rx[i]) # normalize to median
        t.append(np.arange(0, time[i], time[i]/len(Rx[i])))
        print(len(Rx[i] ))
        Rx[i] = Rx[i] [ignore_samples[i]:]
        print(len(t[i] ))
        t[i] = t[i] [ignore_samples[i]:]
        axs[i].plot(t[i], Rx[i])
        axs[i].set(xlabel='time [s]', ylabel='normalized RSS [dB]')
        axs[i].set_title(labels[i])
        axs[i].grid()
        axs[i].set_ylim([-30,10])
    plt.show()
    return Rx



def rayleigh_cdf(data, labels):
    # Show empirical CDF and compare with Rayleigh distribution
    plt.figure(figsize=(9, 7))
    for i, data in enumerate(data):
        xcdf, ycdf = ecdf(data, 50)
        plt.semilogy(xcdf, ycdf, label=labels[i])
    xr = np.arange(np.power(10.0, -30/20), np.power(10.0, 10/20), 0.1)
    yr = sst.rayleigh.cdf(xr)
    xr = 20.0*np.log10(xr/sst.rayleigh.median())
    plt.semilogy(xr,yr, label='Rayleigh')
    plt.plot([0,0],[0.0001,1], label='no fading')
    plt.plot([-30,10],[0.5,0.5], label='50%')
    plt.ylim(0.001,1)
    plt.xlim(-30,10)
    plt.xlabel('Relative loss [dB], normalized (0 dB at 50%)')
    plt.ylabel('Cumulative probability (%)')
    plt.title('CDF')
    plt.legend()
    plt.grid()
    plt.show()

# Show empirical CDF and compare with Rayleigh and Rice distributions
def rice_cdf( r, a, sigma ): # adjusting sst.rice implementation
    return sst.rice.cdf(r, a/sigma, scale=sigma)
def rice_median( a, sigma ):
    return sst.rice.median( a/sigma, scale=sigma)
def rayleigh_cdf(r, sigma): # the same as sst.rayleigh.cdf for sigma=1
    return 1 - np.exp(-r**2/(2*sigma**2))