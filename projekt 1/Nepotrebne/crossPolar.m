
P_0 = p_w

XPD_0 = 30;

Q = -10 * log10((0.7* eta)/P_0)
C = XPD_0 + Q;

eta = 1 - exp(-0.2 * P_0^0.75)