
clear all
close all


fadeMargin = [1:1:20];
L_rain = ones(1,length(fadeMargin)) .* 4.0075;
frequency = 10.8;

p = rainProbability(L_rain, frequency, fadeMargin)

semilogy(fadeMargin,p)
grid on
