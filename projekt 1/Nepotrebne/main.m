%% Parametry - radio

f_b = 40e6;                     % [bit/s] Transmission capacity pro full duplex
constelationPoints = 4;        % [-] Pocet bodu v konstelaci
% constelationPoints = 16;
FEC = 223/255;                  % [-] Forward Error Correction (pouzito RS(255,239)) 
BW = 56e6;                      % [Hz] Sirka pasma 
T = 288.15;                     % [K] Teplota - uvazujeme 15 C
frequency = 11e9;               % [Hz] Frekvence vysilace
Eb_N0 = 10^(10.5/10);           % [-] Spocteno pomoci funkce ExactBER pro modulaci 16QAM a BER 1e-6
% Eb_N0 = 10^(14.3/10);
Pt = -20;                       % [dBm] Vykon transcieveru
G_ant = 40;                     % [dBi] Gain anteny - uvazujeme Alcoma talir prumer 1.2m

%% Parametry - poloha
distance = 7.4;                 % [km] Vzdalenost mezi spoji
h1 = 392;                       % [m n.m.] Vyska prvni anteny
h2 = 389;                       % [m n.m.] Vyska druhe anteny

%% Parametry - destove ztraty
R = 55;                         % [mm/h] Rain rate pro danou oblast a pravdepodobnost deste
k_v = 0.01731;                  % [-] Rain konstanta
alpha_v = 1.1617;               % [-] Rain konstanta (vertikalni polarizace)

%% Parametry - ztraty v atmosferickych plynech
gamma_a = 0.003;                % [dB/km] Konstanta pro vypocet ztraty v atmosferickych plynech (velka prezentace slide 21)

%% Parametry - pravdepodobnosti
d_N1 = 3.23093e+01;             % [-] Refractivity gradient  (ITU-R P.453) (Parametr pro cross polarization)

K_n = 1;                      % [-] parametr pro vypocet propagation effects (tabulka slide 14) (5.5 pro 16QAM, 1 pro QPSK)
% K_n = 5.5;
%% Konstanty
k = 1.38e-23;                   % [m2 kg s^-2 K^-1] Boltzmannova konstanta


%% Vypocet receiver sensitivity
C_N = Eb_N0 * (f_b / BW);                                       % [-] Carrier to noise ratio

N = k * T * BW;                                                 % [dB] Noise power
receiverSensitivity = C_N * N;                                  % [dB] Citlivost prijimace
receiverSensitivity_dbm = 10*log10(receiverSensitivity) + 30;   % [dBm] Citlivost prijimace


%% Vypocet celkoveho utlumu (vzdalenost, dest, plyny)
lambda = 3e8 / frequency;                                       % [m] Vlnova delka
r = 1./(0.477*distance.^(0.633)*R^(0.073*alpha_v)*(frequency/1E9)^(0.123)...
    -10.579*(1-exp(-0.024.*distance)));

L_dist = 32.4 + 20 * log10(frequency/1e6) + 20*log10(distance); % [dBm] Ztraty zavisle na vzdalenosti mezi prijimacem a vysilacem
L_rain = distance * k_v * R^alpha_v * r;                        % [dBm] Ztraty vlivem deste
L_gas = gamma_a * distance;                                     % [dBm] Ztraty v atmosferickych plynech

L_cable = 0;                                                    % [dBm] Ztraty v privodnim kabelu k antene - NEUVAZUJEME

Loss_all = L_dist + L_rain + L_gas + L_cable;                   % [dBm] Soucet vsech ztrat

%% Vypocet Link budget
Pr = Pt + 2 * G_ant - Loss_all;                                 % [dBm] Vykon, ktery dorazi na prijimac
linkBudget = Pr - receiverSensitivity_dbm                       % [dBm] Link budget

%% Pravdepodobnosti - vliv cross-polarizace (slide 13)

fadeDepth = linkBudget;                                         % [dBm] fadeDepth urcuje, kolik dBm si muzeme dovolit ztratit. Uvazujeme tedu link budget jako maximalni ztratu.
K = 10^(-4.6-0.0027 * d_N1);                                    % 
epsylon_p = abs(h1 - h2)/distance;                              % [m] Zmena nadmorske vysky ku vzdalenosti anten

p_w = K * distance ^ 3.1 * (1 + abs(epsylon_p))^-1.29 * ...     % [-] Procentualni podil casu, kdy je ztrata v kanalu vetsi nez "fadeDepth" (linkBudget)
    frequency^0.8 * 10^(-0.00089 * min(h1, h2) - (fadeDepth / 10)); 

P_0 = p_w / 100;                                                % [% 0-100] Procentualni podil casu, kdy je ztrata v kanalu vetsi nez "fadeDepth" (linkBudget)


XPD_0 = 30;                                                     % [dB] Odecteno z dataheetu anteny (XPD_g = 25; 25 + 5 = 30dB)
eta = 1 - exp(-0.2 * (P_0^0.75));                               % Pomocny vypocet (slide 13)
Q = -10 * log10((0.7 * eta)/P_0);                               % Pomocny vypocet (slide 13)
C = XPD_0 + Q;                                                  % Pomocny vypocet (slide 13)

M_XPD = C - C_N;                                                % Pomocny vypocet (slide 13) 
P_XP = P_0 * 10^(-M_XPD/10);                                    % [% 0-100] Pravdepodobnost vypadku vlivem cross-polarizace

%% Pravdepodobnosti - propagation effects (slide 14)

tau_m = 0.7*((distance/50)^1.3);                                % [ns] Stredni casove zpozdeni mezi vysilacem a prijimacem

symbolRate = f_b / (log2(constelationPoints) * FEC);            % [symbol/s] Pocet symbolu vyslanych za 1s
baudPeriod = (1 / symbolRate)*10^9;                             % [ns] Perioda mezi symboly

P_S = 2.15 * eta * K_n*(tau_m^2/baudPeriod^2);                  % [% 0-100] Pravdepodobnost vypadku pro selective outage

%% Pravdepodobnosti pro dest
P_R = 0.003;                                                    % [% 0-100] Pravdepodobnost vypadku vlivem deste
                                                                % Hodnota % 0.003 je nastavena natvrdo - Odviji se od ni potom vypocet ztrat vlivem deste

%% Pravdepodobnosti - celkovy soucet
P_all = P_XP + P_S + P_R                                        % [% 0-100] Vysledna pravdepodobnost vypadku spojeni
