%% Vypocet prisrustku vysky vlivem prohnut zeme na danych vzsalenostech na trase

d1 = [4.7, 25.6 14.08 21.3 11 6];   % Vzdalenosti od pocatku, ve kterych chci vypocitat prohnuti Zeme
                                    % Bereme prumet pozice vyslacu na vzsusnou caru

dist_whole = 21;                    % Celkova vzdalenost spoje (vzdusnou carou)
k = 4/3;                            % Koeficient k (viz slidy)

Re = 6371;                          % Polomer Zeme
Reff = k * Re;                      % Efektivni polomer Zeme
d2 = dist_whole - d1;               % Vypocet d2 do vzorce

x = (d1 .* d2) / (2*Reff);          % Vypocet "prohnuti"
x = x .*1000                        % Prevod z km na m