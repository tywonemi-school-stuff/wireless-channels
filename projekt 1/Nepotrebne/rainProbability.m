function [prob] = rainProbability(A001, f, Ap)
%RAINPROBABILITY Summary of this function goes here
%   Detailed explanation goes here


C0 = 0.12+0.4*((log10(f/10))^0.8);
C1 = (0.07^C0)*(0.12^(1-C0));
C2 = 0.855*C0+0.546*(1-C0);
C3 = 0.139*C0+0.043*(1-C0);

syms p;
prob = zeros(1,length(A001));

for n = (1:length(A001))
    Ap(n)
    eqn = Ap(n) ==  C1 * p ^ (-(C2+C3*log10(p))) * A001(n);

    prob(n) = vpasolve(eqn,p);
end

end

