f = 10.8e9;
frequency = f;
alpha_v = 1.1617;
R = 32;
p = 0.01




gamma = 0.003;
distance = 7;
r = 1./(0.477*distance.^(0.633)*R^(0.073*alpha_v)*frequency^(0.123)...
    -10.579*(1-exp(-0.024.*distance)));  

syms p

A001 = gamma * distance * r
rainProbability(A001, f, p)

