---
title: "Šíření vln pro bezdrátové spoje projekt 1"
date: "2021-03-25"
lang: "cs"
author-meta: asdasdasd
titlepage: true
titlepage-color: "4277BF"
titlepage-text-color: "FFFFFF"
# logo: logoFEL.pdf
mybackground: titlepage.jpg
author: [Bc. Tomáš Kestřánek, Bc. Jakub Horáček, Bc. Emil J. Tywoniak]
header-includes:
    \usepackage[binary-units=true]{siunitx}
    \DeclareSIUnit{\belmilliwatt}{Bm}
    \DeclareSIUnit{\beliso}{Bi}
    \DeclareSIUnit{\dBm}{\deci\belmilliwatt}
    \DeclareSIUnit{\dBi}{\deci\beliso}
...

# Zadání

Cílem této práce je navrhnout mikrovlnný spoj mezi rozhlednou Městská hora v Berouně a budovou ČVUT FEL v Pražských Dejvicích podle následujících parametrů:

+ Úroveň spolehlivosti je alespoň \SI{99.99}{\percent}
+ Chybovost BER = $\num{1e-6}$
+ Maximální EIRP = \SI{20}{\dBm} (\SI{100}{\milli\watt})
+ Použité komponenty jsou od firmy [Alcoma](www.alcoma.cz)

Při daných parametrech je snaha maximalizovat přenosovou kapacitu spojení a v maximální míře využít stávající telekomunikační infrastrukturu. Všechny výpočty vycházejí z doporučení ITU-R P.530 a všech doporučení s ním souvisejících.

# Vlastnosti spoje

Frekvenci jsme vybrali takovou, že ztráty deštěm nezpůsobují značné výpadky, a jiné mechanismy se už moc neprojevují. Vertikální polarizace dále snižuje ztráty deštěm. Pro jednoduchost jsme vybrali jednu anténu pro všechny komunikační body. Vybrali jsme anténu s největším ziskem pro vyšší rezervu na úniky. Šířku kanálu jsme pro maximální průtok, případně maximální rezervu na úniky, vybrali největší možnou technicky, a ověřili, že v licencovaných pásmech na naší pracovní frekvenci je dostupná. Původně jsme navrhovali spoj pro QAM, z čehož jsme dopočítali, že dosáhneme lepší rezervy na úniky s QPSK. Toto jsou tedy finální parametry našeho spoje:

- Frekvence \SI{10.8}{\giga\hertz}, vertikální polarizace
- Největší anténa \SI{1.2}{\meter}, gain \SI{40}{\dBi}
- Šířka \SI{56}{\mega\hertz}
- QPSK 82Mbps - maximum specifikované výrobcem

Z požadované BER $\left(\frac{E_b}{N_0}\right)$, zvolené šířky pásma $B$ [Hz] a přenosové kapacity $f_n$ [Hz] jsme určili citlivost přijímače: 
$$\frac{C}{N} = \frac{E_b f_b}{N_0 B}$$
kde $N$ je šum pozadí daný teplotou $T$ [K], Boltzmanovou konstantou $k$ a lze určit následovně:
$$N = kTB$$
Citlivost přijímače vychází \SI{-84.3669}{\dBm} pro naši konfiguraci. To nám dává dostatečný prostor pro nízké pravděpodobnosti výpadku všemi mechanismy.

# Umístění vysílačů

Umístění vysílačů jsme volili takové, abychom co nejvíce využili stávající telekomunikační architekturu, tj. pro umístění antén jsme využili již stojící stavby, které na sobě už antény mají. Předpokládáme tak, že přidat další bude minimální problém.

Geografickou polohu jsme vybírali tak, abychom co nejméné navyšovali vzdálenost mezi Berounem a Dejvicemi (oproti vzdálenosti vzdušnou čarou) a zároveň jsme vyhověli požadavkům na neporušení první Fresnelovy zóny. 
 
Vybrané stavby jsou zobrazeny na obrázku a v tabulce níže.

![Spoj](Report/mapaSCisly.png "")


Číslo bodu   Jméno stavby               h [m] Výška stožáru [m] Poloha                                               
------------ -------------------------- ----- --                ---------------------------------------------------- 
1            Rozhledna Měststká hora    292   13                \SI{49.9626347}{\degree}N, \SI{14.0651133}{\degree}E 
2            Rozhledna Lhotka u Berouna 462   65                \SI{49.9960786}{\degree}N, \SI{14.1073292}{\degree}E 
3            Vysílač rudná              392   10                \SI{50.0184783}{\degree}N, \SI{14.1957125}{\degree}E 
4            Vysílač Praha - Třebonice  389   15                \SI{50.0512261}{\degree}N, \SI{14.2830172}{\degree}E 
5            Vysílač Strahov            423   63                \SI{50.0798536}{\degree}N, \SI{14.3758333}{\degree}E 
6            ČVUT FEL Dejvice           253   40                \SI{50.1030994}{\degree}N, \SI{14.3927450}{\degree}E 

Table:  Stavby

Výškový profil trasy bylo nutné pro výpočet Fresnelových zón korigovat podle prohnutí Země. Korekce podle kulového vrchlíku způsobila maximální vzedmutí \SI{11.6}{\meter} uprostřed trasy. První Fresnelova zóna má poloměr \SI{7.15}{\meter}. Ověřili jsme že v každém úseku má signál odstup \SI{6}{\meter} k nejvyšší pozemní překážce (po korekci podle kulového vrchlíku).

![Výškový profil bez korekce](Report/profilFinal.png ""){ width=50% }

Číslo spoje    Délka [km]   Rozdíl výšek [$\Delta$ m] 
-----------    ------       ----------           
    1-2        \num{4.81}   \num{-224.5}
    2-3        \num{6.78}   \num{127.5}
    3-4        \num{7.25}   \num{-2}
    4-5        \num{7.37}   \num{-19}
    5-6        \num{2.88}   \num{170}

Table: Rozdíly výšek bodů spoje


# Výkonová bilance

Číslo spoje    d [km]     FSL [dB]       Ztráty plynu [dB]  Deterministické ztráty [dB]
-----------    ------     --------       ------             --------
    1-2        \num{4.81} \num{126.7114} \num{0.0144}       \num{126.7258}
    2-3        \num{6.78} \num{129.6879} \num{0.0203}       \num{129.7083}
    3-4        \num{7.25} \num{130.2764} \num{0.0218}       \num{130.2982}
    4-5        \num{7.37} \num{130.4119} \num{0.0221}       \num{130.4340}
    5-6        \num{2.88} \num{122.2412} \num{0.0086}       \num{122.2499}

Table:  Deterministické ztráty na spojích

Číslo spoje d [km] Fade margin        Multipath               XPD                     ISI                         Deštěm
----------- ------ --------           -------------------     -------------------     --------------------------- -------------
    1-2     4,81   \SI{17.64}{\dBm}   \SI{1.13e-8}{\percent}  \SI{1.34e-9}{\percent}  \SI{1.15e-12}{\percent}     \SI{<0.001}{\percent}
    2-3     6,78   \SI{14.66}{\dBm}   \SI{1.65e-7}{\percent}  \SI{1.97e-8}{\percent}  \SI{2.10e-11}{\percent}     \SI{<0.001}{\percent}
    3-4     7,25   \SI{14.07}{\dBm}   \SI{8.04e-6}{\percent}  \SI{9.57e-7}{\percent}  \SI{4.62e-10}{\percent}     \SI{<0.001}{\percent}
    4-5     7,37   \SI{13.93}{\dBm}   \SI{2.29e-6}{\percent}  \SI{2.72e-7}{\percent}  \SI{1.88e-10}{\percent}     \SI{<0.001}{\percent}
    5-6     2,88   \SI{22.12}{\dBm}   \SI{6.75e-10}{\percent} \SI{8.02e-11}{\percent} \SI{3.65e-14}{\percent}     \SI{<0.001}{\percent}

Table:  Pravděpodobnosti výpadku

Sloupec označen "Multipath" značí výpadky způsobené vícecestným šířením, "XPD" výpadky způsobené případnými přeslechy mezi polarizacemi, "ISI" výpadky způsobené mezisymbolovou inteferencí. Ztráty v přívodu do antény jsme zanedbali. Výpočty jsou komentovány v přiloženém MATLAB skriptu `computeAll.m`.

# Závěr

Podařilo se nám navrhnout mikrovlnný spoj mezi rozhlednou Mětská hora v Berouně a budovou ČVUT FEL v Pražských Dejvicích tak, aby splňoval všechny podmínky ze zadání. Spoj má pět úseků a využívá pouze existující infrastrukturu. Pravděpodobnost výpadku byla vypočtena podle doporučení ITU-R P.530. Pravděpodobnosti výpadku různých mechanismů mají s různými předpoklady korelace různé vzorce. Všechnny jsou ale modifikací horního odhadu pravděpodobnosti nastání jednoho z několika jevů s neznámou závislostí, tedy sumou jejich pravděpodobností. Ověřili jsme, že korekce tuto pravděpodobnost jen snižují, a to o zanedbatelné hodnoty. Proto je náš postup platný. Pravděpodobnost výpadku je pak \SI{6.18e-3}{\percent}.

Podařilo se nám tedy navrhnout spoj splňující spolehlivost \SI{99.99}{\percent} a to s přenosovou kapacitou 86Mbps.

# Použitá doporučení

- P.530 - obecná metodika
- P.453 - parametry pro křížovou polarizaci
- P.525 - ztráty ve volném prostoru
- P.676 - ztráty atmosférickými plyny
- P.837 - ztráty deštěm
- P.838 - parametry ztrát deštěm v závislosti na polarizaci

