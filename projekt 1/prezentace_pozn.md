Poznámky k prezentaci projektu na SBS
===
skupina 6: Tywoniak, Horáček, Kestřánek

Zadání projektu bylo následující:
--
 - dosáhnout spojení na trase rozhledna Beroun - budova FEL s dostupností 99.99% času v roce
 - dále omezení:
   -  max výkon 20dBm
   -  antény Alcoma ALxF -> omezující co do frekvencí, modulací, kódování, citlivosti

problémy:
---
 - nemáme LoS mezi počátkem a cílem
 - silné omezení vyzařovaného výkonu  
  -> potřeba několika bodů po cestě pro retranslaci signálu

metody řešení:
---
 - pracovní postup zhruba podle zadávací prezentace, doplněno o další ITU-R doporučení
 - při výběru retranslačních bodů důraz na maximální využití stávající infrastruktury
 - implementace v MATLABu

výškový profil
---
 - Lhotka - bod 3
 - Strahov - bod 5

vlastnosti spoje
---
celkové pravděpodobnost: 6.1767e-03 %








