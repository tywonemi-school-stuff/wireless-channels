clear all;close all;
%% Vypocet FadeMargin a Pravdepodobnosti pro celou prenosovou cestu


distance = [4.81,6.776,7.251,7.365,2.875];                      % [km] Vzdalenosti mezi jednotlivymi vysilaci
h1 = [305,529.5,402,404,423];                                   % [m] Nadmorska vyska vysilacu (1 az n-1) (korigovana vrchlikem)
h2 = [529.5,402,404,423,253];                                   % [m] Nadmorska vyska vysilacu (2 az n)   (korigovana vrchlikem)


[fadeMargins, probabilities] = computeAll(distance, h1, h2);    % Vypocet FadeMargin a Pravdepodobnosti vypadku pro kazdy hop

ProbabilitySummed = sum(probabilities)                          % [0-1] Celkova pravdepodobnost vypadku pres vsechny hopy