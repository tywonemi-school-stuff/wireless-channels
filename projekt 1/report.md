---
title: "Šíření vln pro bezdrátové spoje projekt 1"
date: "2021-06-16"
lang: "cs"
author-meta: asdasdasd
titlepage-color: white
mybackground: titlepage.jpg
header-includes:
    \usepackage[binary-units=true]{siunitx}
    \DeclareSIUnit{\belmilliwatt}{Bm}
    \DeclareSIUnit{\beliso}{Bi}
    \DeclareSIUnit{\dBm}{\deci\belmilliwatt}
    \DeclareSIUnit{\dBi}{\deci\beliso}
...

# Zadání

- max výkon $\SI{20}{\dBm}$ na ose spoje
- maximální průtok bez multiplexace
- antény řady Alcoma ALxF
- Beroun, rozhledna na Městské hoře, Lat: \SI{49.9626386}{\degree}N, Lon: \SI{14.0650983}{\degree}E, výška \SI{14}{m} nad
terénem
- Praha Dejvice, budova FEL, Lat: \SI{50.103153}{\degree}N, Lon: \SI{14.392759}{\degree}E, výška budovy \SI{40}{m}

# Problémy

- nemáme line-of-sight
- nízký výkon, potřeba několik bodů

# Metody řešení

- korektní metody dle doporučení ITU-R
- maximální využití existujících instalačních bodů
- znovu použitelný MATLAB skript s vektorovým vyhodnocením bodů

# Výškový profil

- Strahov, Lhotka u Berouna: značná výška telekom stožárů
- mezi nimi víceméně plochý terén, mezispoje jen kvůli útlumu prostředí

![Spoj](Report/mapaFinal.png "")

# Výškový profil

- Strahov, Lhotka u Berouna: značná výška telekom stožárů
- mezi nimi víceméně plochý terén, mezispoje jen kvůli útlumu prostředí

![Výškový profil bez korekce](Report/profilFinal.png "")

# Vlastnosti spoje

- Frekvence \SI{10.8}{\giga\hertz}
- Největší anténa \SI{1.2}{\meter}, gain \SI{40}{\dBi}
- Šířka \SI{56}{\mega\hertz}
- QPSK \SI{82}{\mega\bit\per\second} - maximum antény

# Vlastnosti spoje

Nejztrátovější spoj:

- \SI{7.25}{\kilo\meter}
- Ztráty \SI{133.3}{dB}
- Fade margin \SI{14.06}{dB}
- Pravděpodobnost výpadku 0.0019$\,\%$

# Otázky
