function [Ap] = rainLoss(f, A001, p)

%% Vypocet Ap na zaklade frekvence (f), FadeMarginu (A001) a pravdepodobnosti vypadku vlivem deste (p)
% Vypocet vychazi ze slidu 11

C0 = 0.12+0.4*((log10(f/10))^0.8);
C1 = (0.07^C0)*(0.12^(1-C0));
C2 = 0.855*C0+0.546*(1-C0);
C3 = 0.139*C0+0.043*(1-C0);

Ap =  C1 * p ^ (-(C2+C3*log10(p))) * A001;

end

